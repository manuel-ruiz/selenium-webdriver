package es.selenium.testng;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Login extends Base{

	@BeforeMethod 
	public void beforeMethod(Method method) {
		System.out.println("[TestNG] -> @BeforeMethod");
		String testName=method.getName();
		testInfo=reports.createTest(testName);
		testInfo.createNode(testName);
	}
  

	@Parameters({ "user", "password" })
	@Test 
	public void login(String user, String password) {
		driver.findElement(By.linkText("My Kamstrup")).click();
		
		// Sikuli for the image recognition 
		myScreen = new Screen();
		Pattern eks = new Pattern(System.getProperty("user.dir") + "\\img\\eks.jpg");
		try {
			
			myScreen.wait(eks, 10);
			myScreen.click(eks);
			Thread.sleep(3000);
			
		} catch (FindFailed | InterruptedException e) {
			e.printStackTrace();
		}
		
		// Multiple windows handles
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    
	    try {
			// Switch the focus from one window to another
		    driver.switchTo().window(tabs.get(1)); 
		    
			
			driver.findElement(By.id("userNameInput")).sendKeys(user);
			driver.findElement(By.id("passwordInput")).sendKeys(password);
		
			driver.findElement(By.id("submitButton")).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	    String expectedTitle = "device-list";
	    Assert.assertEquals(driver.getTitle(), expectedTitle);
	}
}
