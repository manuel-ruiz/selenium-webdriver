package es.selenium.testng;

import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;

public class UtiliDriver extends Base{

	@BeforeMethod 
	public void BeforeMethod(Method method) {
		System.out.println("[TestNG] -> @BeforeMethod");
		String testName=method.getName();
		testInfo=reports.createTest(testName);
		ExtentTest node = testInfo.createNode("asds");
		node.info("details node 1");
		
	}
  
	@Test (dependsOnMethods={"es.selenium.testng.Login.login"})
	public void tabUtilyDriver() {
		
		
		ExtentTest node = testInfo.createNode("Node for tabUtilyDriver 1");  // level = 1
		node.pass("details node 1");
		node = testInfo.createNode("Node for tabUtilyDriver 2");  // level = 1
		node.pass("details node 2");
		
		MediaEntityModelProvider mediaModel=null;
		try {
			mediaModel = MediaEntityBuilder.createScreenCaptureFromPath("C:\\Users\\majr\\Desktop\\logos\\ninja2.png").build();
		} catch (IOException e) {
			e.printStackTrace();
		}
		testInfo.info("details", mediaModel);
		Assert.assertTrue(true);
	  
	}	
}
