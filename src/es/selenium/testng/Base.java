package es.selenium.testng;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.Screen;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Base{

	public static ExtentReports reports;
	public static ExtentTest testInfo;
	public static ExtentTest testInfoSkupped;
	public static ExtentHtmlReporter htmlReporter;
	
	public static WebDriver driver;
	public static Screen myScreen;
	
	@BeforeSuite
	public void setUp() {
		System.out.println("[@BeforeSuite] -> setUp()");
		
		/**
		 * Extent Report
		 */
		htmlReporter = new ExtentHtmlReporter(new File(System.getProperty("user.dir") + "/AutomationReport-MyKamstrup.html"));
		htmlReporter.loadXMLConfig(new File(System.getProperty("user.dir") + "/config/Extent-config.xml"), true);

		reports = new ExtentReports();
		reports.setSystemInfo("Environment",  "Stable");
		reports.setSystemInfo("Role",  "N/A");
		reports.attachReporter(htmlReporter);
		reports.setAnalysisStrategy(AnalysisStrategy.TEST);
		
		/**
		 * WebDriver
		 */
		try {
				
			System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");         
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.get("https://udgiv.kamstrup.com/en-en");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	  
	}
  
	@AfterMethod 
	public void AfterMethod(ITestResult result) {
		if (result.getStatus()==ITestResult.SUCCESS) {
			testInfo.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " Test Case PASSED", ExtentColor.GREEN));
			testInfo.log(Status.PASS, "The Test Method name as : " + result.getName() + " is passed");
		}else if (result.getStatus()==ITestResult.FAILURE) {
			testInfo.log(Status.FAIL, "The Test Method name as : " + result.getName() + " is failed");
			testInfo.log(Status.FAIL, "Test failure : " + result.getThrowable());
		}else if (result.getStatus()==ITestResult.SKIP) {
			testInfo.log(Status.SKIP, "The Test Method name as : " + result.getName() + " is skipped");
		}
	}

	@AfterSuite
	public void tearDown() {
		System.out.println("[@AfterSuite] -> tearDown()");
		reports.flush();
		driver.quit();
	}
	
	
}
